
/* setup CSRF for ajax */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


/**
 * Comment editor with preview
 */
$(".editor").each(function(){
    var $editor = $(this);
    var $text = $editor.find(".editor-text");
    var $preview = $editor.find(".editor-preview").hide();

    var $btnWrite = $editor.find(".editor-btn-write");
    var $btnPreview = $editor.find(".editor-btn-preview");

    $btnWrite.on("click",function(e) {
        e.preventDefault();
        $btnWrite.addClass("active");
        $btnPreview.removeClass("active");
        $text.show();
        $preview.hide();
    });

    $btnPreview.on("click",function(e) {
        e.preventDefault();
        $btnWrite.removeClass("active");
        $btnPreview.addClass("active");
        $text.hide();
        $preview.show();

        $preview.html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw text-muted center-block"></i>');
        $.post(POST_PREVIEW_URL, {'md': $text.val()}, function(result) {
            $preview.html(result);
        });
    });

});
