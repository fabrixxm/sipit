/* pick a color */
var colors = ['b60205','d93f0b','fbca04','0e8a16','006b75','1d76db','0052cc','5319e7'];

var pickacolortmpl = "<div class='pick-a-color-colors'>";
for(var k=0; k<colors.length; k++) {
    pickacolortmpl += "<a href='#' class='pick-a-color-box label label-default' data-color='"+colors[k]+"' style='background-color:#"+colors[k]+"'><i class='fa fa-circle-thin'></i></a>";
}
pickacolortmpl += "</div>";

$("input.pick-a-color").each(function() {
    var theinput = $(this);
    var theselector = $(pickacolortmpl);
    theinput.hide().after(theselector);
    console.log(theselector);
    theselector.on('click', 'a', function(e) {
        e.preventDefault();
        e.stopPropagation();
        theinput.val($(this).data('color'));
        theselector.find('a > i.fa').removeClass('fa-circle fa-circle-thin').addClass('fa-circle-thin');
        $(this).find('i.fa').removeClass('fa-circle-thin').addClass('fa-circle');
    });
});


