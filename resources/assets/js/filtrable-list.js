
/* icontains */
$.expr[':'].icontains = function(obj, index, meta, stack){ return (obj.textContent || obj.innerText || jQuery(obj).text() || '').toLowerCase().indexOf(meta[3].toLowerCase()) >= 0; };

/* filter lists */
$("ul.filtrable").each(function() {
    var thelist = $(this);
    var thenotfound = $(this).find('.notfound').hide();
    thelist.find(".search-box > input")
        .on("keyup", function(e) {
            if (e.keyCode==27) $(this).val(""); // clear on ESC
            var term = $(this).val();
            var found = 0;

            thelist.find("li").not(".notfound").each(function(){
                var li = $(this);
                if (term === "" || li.find('*:icontains("'+term+'")').length > 0) {
                    li.show();
                    found++;
                } else {
                    li.hide();
                }
            });

            if (found==0) {
                thenotfound.show();
            } else {
                thenotfound.hide();
            }
        })
        .val("");
});

