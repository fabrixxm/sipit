@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <h3><span class="text-danger">403</span> - Ops. <small>You can't open this.</small></h3>
            <hr>
        </div>
    </div>
</div>
@endsection
