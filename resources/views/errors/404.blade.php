@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <h3><span class="text-info">404</span> - Ops. <small>I can't find anything here.</small></h3>
            <hr>
        </div>
    </div>
</div>
@endsection
