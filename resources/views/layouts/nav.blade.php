@extends('layouts.app')

@section('precontent')
    <div class="container">
        <ul class="breadcrumb">
            @yield('nav')
        </ul>
    </div>
@endsection
