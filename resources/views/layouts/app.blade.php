<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SiPIT @yield('title')</title>

    <link rel="icon" href="{{ url('/').'/favicon.png' }}" type="image/png" >
    <link rel="apple-touch-icon" href="{{ url('/').'/favicon.png' }}">
    <link href="{{ url('/').'/css/vendor.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/app.css' }}" rel="stylesheet">

    <script>
        @if(!isset($project))
        POST_PREVIEW_URL = "{{ route('comment.preview', "null") }}";
        @else
        POST_PREVIEW_URL = "{{ route('comment.preview',  $project->slug) }}";
        @endif
    </script>

</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('/').'/images/sipit.svg' }}" width="24" height="24" style="display: inline-block">
                    SiPIT
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Projects</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <!-- <li><a href="{{ url('/register') }}">Register</a></li> -->
                    @else
                        @if (Auth::user()->hasRole('admin'))
                        <li class="@yield('nav.users')">
                            <a href="{{ route('users.index')}}">
                                <i class="fa fa-users" aria-hidden="true"></i> Users
                            </a>
                        </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <img src="{{ route('avatar', [Auth::user()->id,16]) }}" class="avatar" width="16" height="16">
                                {{ Auth::user()->name }}
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if(session('alert'))
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-{{ session('alert')['type'] }} .alert-dismissible fade in out" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('alert')['message'] }}
                </div>
            </div>
        </div>
    </div>
    @endif

    @yield('precontent')
    @yield('content')

    <script src="{{ url('/').'/js/vendor.js' }}"></script>
    <script src="{{ url('/').'/js/app.js' }}"></script>

    @yield('scripts')
</body>
</html>
