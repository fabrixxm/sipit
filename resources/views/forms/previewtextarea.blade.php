{{--
    Bootstrap textarea form field with nav tabs to load markdown preview via ajax.
    Use as:

        @include("forms.previewtextarea", ['name'=>'field name', 'text'=>'textarea value'])

    $name will be used as label
    $text is optional

--}}
<div class="form-group editor">
    <label for="description" class="control-label">{{ ucfirst($name) }}</label>
    <ul class="nav nav-tabs">
        <li class="editor-btn-write active"><a href="#">Write</a></li>
        <li class="editor-btn-preview"><a href="#">Preview</a></li>
    </ul>
    <div class="editor-preview"></div>

    <textarea rows="6" class="form-control editor-text" id="{{ $name }}" name="{{ $name }}" cols="50">@if(isset($text)){{ $text }}@endif</textarea>
</div>
