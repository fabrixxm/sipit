@extends('user.base')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                {!! BootForm::open(['model' => $user, 'store' => 'users.store', 'update' => 'users.update']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title">@yield('form.title')</h3>
                    </div>
                    <div class="panel-body">
                        {!! BootForm::text('name') !!}
                        {!! BootForm::text('username') !!}
                        {!! BootForm::email('email') !!}
                        {!! BootForm::password('password') !!}
                        {!! BootForm::password('password_confirmation', 'Confirm password') !!}

                        {!! BootForm::checkboxes('roles[]', "User role", $roles, (($user) ? $user->roles()->pluck('role_id')->toArray():[]) ) !!}
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
