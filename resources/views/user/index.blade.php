@extends('user.base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Users
                        <a class="pull-right btn btn-default btn-xs"
                         href="{{ route('users.create') }}" title="Add new user">
                            <i class="fa fa-user-plus"></i>
                        </a>
                    </div>

                    <!-- <div class="panel-body">

                    </div> -->
                    <ul class="list-group">
                    @foreach($users as $user)
                        <li class="list-group-item">
                            <a href="{{ route("users.edit", $user->id) }}">
                                <img src="{{ route('avatar', [$user->id, 16]) }}" width="16" height="16">
                                {{ $user->name }}
                                <span class="text-muted">{{ $user }}</span>
                            </a>
                            <span class="pull-right">
                                @if($user->hasRole('admin'))
                                        <i class="fa fa-key" title="administrator"></i>
                                @endif
                                <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash" title="Delete user"></i></a>
                            </span>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
