@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                @php( $view = ['issue.update', $project->slug] )
                {!! BootForm::open(['model' => $issue, 'store' => $view, 'update' => $view]) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title">@yield('form.title')</h3>
                    </div>
                    <div class="panel-body">
                        {!! BootForm::text('title') !!}

                        @if(is_null($issue))
                            <!-- { !! BootForm::textarea('description') !! } -->
                            @include('forms.previewtextarea', ['name'=>'description'])
                        @endif
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
