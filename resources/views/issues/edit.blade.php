@extends('issues.form')

@section('title')
 - Edit issue #{{ $issue->number }} - {{ $project->slug }}
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li><a href="{{ route('project',  $project->slug) }}">{{ $project->slug }}</a></li>
    <li><a href="{{ route('issue', [$project->slug, $issue->number]) }}">Issue #{{ $issue->number }}</a></li>
    <li class="active">edit</li>
@endsection

@section('form.title')
    <i class="fa fa-info-circle text-warning"></i> Edit issue #{{ $issue->number }}

@endsection
