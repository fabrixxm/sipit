<div class="row">
    <div class="col-sm-11 col-sm-offset-1">
        <div class="timeline">
            <div class="iconbadge bg-muted text-muted">
                <i class="fa fa-link"></i>
            </div>
                <strong>
                    <img src="{{ route('avatar', [$comment->user()->id,16]) }}" class="avatar" width="16" height="16">
                    {{ $comment->user()->name }}
                </strong>
                    @php($ref_c = $comment->getReferrer())
                    @php($ref_i = $ref_c->issue())
                    referenced this issue from

                    @if($ref_i->isOpen())
                        <i class="fa fa-info-circle text-warning" title="Open"></i>
                    @else
                        <i class="fa fa-check-circle text-muted" title="Close"></i>
                    @endif

                    <a href="{{ route('issue', [$ref_i->project()->slug, $ref_i->number]) }}#comment-{{ $ref_c->id }}">
                        {{ $ref_i->title }} • #{{ $ref_i->number }}
                    </a>
                    &nbsp;
                    <span title="{{ $comment->created_at }}">{{ $comment->created_at->diffForHumans() }}</span>
        </div>
    </div>
</div>
