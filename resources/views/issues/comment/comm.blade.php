<!-- comment element -->
<div class="row" id="comment-{{ $comment->id }}">
    <div class="col-sm-1 hidden-xs">
        <img src="{{ route('avatar', [$comment->user()->id, 48]) }}"  class="avatar" width="48" height="48">
    </div>
    <div class="col-sm-11">
        <div  id="comment-{{$comment->id}}" class="panel panel-default with-avatar timeline {{ ($comment->canEdit(Auth::user())?"editable":"") }}">
          <div class="panel-heading">
              @if( $comment->canEdit(Auth::user()))
                  <a href="#"
                      class="btn btn-default btn-xs pull-right editable-btn-edit">
                      <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                  </a>
              @endif

              <strong>{{ $comment->user()->name }}</strong> commented <a href="#{{ $comment->id }}" title="{{ $comment->created_at }}">{{ $comment->created_at->diffForHumans() }}</a>
              @if( $comment->created_at != $comment->updated_at )
                   &centerdot; edited <span title="{{ $comment->updated_at }}">{{ $comment->updated_at->diffForHumans() }}</span>
              @endif
          </div>
          <div class="panel-body">
            <!-- @ markdown($comment->text) -->
            {% $comment->parsedMarkdown() %}
          </div>
        </div>

        @if( $comment->canEdit(Auth::user()))
        <div id="comment-{{$comment->id}}-editor" class="panel with-nav-tabs panel-default with-avatar editor">
          <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="editor-btn-write active"><a href="#">Write</a></li>
                <li class="editor-btn-preview"><a href="#">Preview</a></li>
            </ul>
          </div>
          {!! BootForm::open(['model'=>$comment, 'update'=>['comment.update', $comment->id]]) !!}
              <div class="panel-body">
                <div class="editor-preview"></div>
                {!! BootForm::textarea('text', false, null, ['class'=>'form-control editor-text', 'rows'=>5]) !!}
              </div>
              <div class="panel-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default pull-right editor-btn-cancel">Cancel</button>
              </div>
          {!! BootForm::close() !!}
        </div>
        @endif
    </div>
</div>
