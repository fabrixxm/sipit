<div class="row">
    <div class="col-sm-11 col-sm-offset-1">
        <div class="timeline">
            <div class="iconbadge bg-info text-info">
                <i class="fa fa-tag"></i>
            </div>
                <strong>
                    <img src="{{ route('avatar', [$comment->user()->id,16]) }}" class="avatar" width="16" height="16">
                    {{ $comment->user()->name }}
                </strong>
                    @php($label = $comment->getLabel())
                    added label
                    <div class="label label-default"
                        style="background-color:#{{ $label->color }}">
                            {{ $label->name }}
                    </div> &nbsp;
                    <span title="{{ $comment->created_at }}">{{ $comment->created_at->diffForHumans() }}</span>
        </div>
    </div>
</div>
