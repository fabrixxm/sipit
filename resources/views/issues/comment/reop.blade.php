<div class="row">
    <div class="col-sm-11 col-sm-offset-1">
        <div class="timeline">
            <div class="bg-warning text-warning iconbadge">
                <i class="fa fa-info"></i>
            </div>
                <strong>
                    <img src="{{ route('avatar', [$comment->user()->id,16]) }}" class="avatar" width="16" height="16">
                    {{ $comment->user()->name }}
                </strong>
                    reopened this issue <span title="{{ $comment->created_at }}">{{ $comment->created_at->diffForHumans() }}</span>
        </div>
    </div>
</div>
