@extends('issues.form')

@section('title')
 - New issue - {{ $project->slug }}
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li><a href="{{ route('project',  $project->slug) }}">{{ $project->slug }}</a></li>
    <li class="active">new</li>
@endsection

@section('form.title')
    <i class="fa fa-info-circle text-warning"></i> New issue

@endsection
