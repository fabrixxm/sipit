@extends('layouts.nav')

@section('title')
 - {{ $project->slug }}
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li><a href="{{ route('project',  $project->slug) }}" class="active">{{ $project->slug }}</a></li>
@endsection

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-9">
            <form method="get">
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Search" name="q" value="{{ $query }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
            <br>
            @if($query !== "" && $query !== "is:open")
                <h5><a href="?"><i class="fa fa-times" aria-hidden="true"></i> Clear current search, filters and sort.</a></h5>
                <br>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="?q={{ Utils::editQuery($query,'is:open') }}" class="flat">
                        <i class="fa fa-info-circle" alt="open issues"></i>
                        <span class="hidden-xs">&nbsp; </span>{{ $project->issues()->isOpen()->count() }}
                        <span class="hidden-xs">Open</span>
                    </a>
                    &nbsp; &centerdot; &nbsp;
                    <a href="?q={{ Utils::editQuery($query,'is:closed') }}" class="flat">
                        <i class="fa fa-check-circle" alt="closed issues"></i>
                        <span class="hidden-xs">&nbsp; </span>{{ $project->issues()->where('status', 'c')->count() }}
                        <span class="hidden-xs">Closed</span>
                    </a>
                    <div class="pull-right">
                        <div class="dropdown flat">
                            <a class="dropdown-toggle" type="button" id="dropdownMenuAuthor"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Author <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu filtrable" aria-labelledby="dropdownMenuAuthor">
                                <div class="search-box">
                                    <input type="text" placeholder="Search users" class="form-control live-search" />
                                </div>
                            @foreach($project->users()->get() as $user)
                                <li><a href="?q={{ Utils::editQuery($query,'author:'.$user->username) }}">{{ $user->name }}</a></li>
                            @endforeach
                            </ul>
                        </div>

                        @if(\App\Label::count())
                        <div class="dropdown flat">
                            <a class="dropdown-toggle" type="button" id="dropdownMenuLabel"
                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Label <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu filtrable" aria-labelledby="dropdownMenuLabel">
                                <div class="search-box">
                                    <input type="text" placeholder="Search labels" class="form-control live-search" />
                                </div>
                                <li><a href="?q={{ Utils::editQuery($query, 'label:none') }}" class="text-muted">unlabeled</a></li>
                                @foreach($project->labels()->get() as $label)
                                    <li><a href="?q={{ Utils::editQuery($query, 'label:'.$label->name)}}">
                                        <div class="pull-right label label-default"  style="background-color:#{{ $label->color }};">&nbsp;</div>
                                        {{ $label->name }}
                                    </a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <div class="dropdown flat">
                            <a class="dropdown-toggle" type="button" id="dropdownMenuSort"
                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Sort <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuSort">
                                <li><a href="?q={{ Utils::editQuery($query,'sort:created-desc')}}">Newest</a></li>
                                <li><a href="?q={{ Utils::editQuery($query,'sort:created-asc')}}">Oldest</a></li>
                                <li><a href="?q={{ Utils::editQuery($query,'sort:comments-desc')}}">Most commented</a></li>
                                <li><a href="?q={{ Utils::editQuery($query,'sort:comments-asc')}}">Least commented</a></li>
                                <li><a href="?q={{ Utils::editQuery($query,'sort:updated-desc')}}">Recently updated</a></li>
                                <li><a href="?q={{ Utils::editQuery($query,'sort:updated-asc')}}">Least recently updated</a></li>
                            </ul>
                        </div>

                    </div>



                </div>

                <div class="list-group issue-list">
                @foreach($issues as $issue)
                    <a href="{{ route("issue", [$project->slug, $issue->number]) }}" class="list-group-item issue">
                        <span class="pull-left issue-status-icon">
                            @if($issue->isOpen())
                                <i class="fa fa-info-circle text-warning" title="Open"></i>
                            @else
                                <i class="fa fa-check-circle text-muted" title="Close"></i>
                            @endif
                        </span>
                        @php( $commentscount = $issue->commentsCount() )
                        @if($commentscount > 0)
                        <span class="badge issue-comments-count"><i class="fa fa-comment-o"></i> {{ $commentscount }}</span>
                        @endif
                        <h4 class="list-group-item-heading">{{ $issue->title }} </h4>

                        @foreach($issue->labels()->get() as $label)
                            <span class="label label-default pull-right issue-label"
                             style="background-color:#{{ $label->color }}">
                                {{ $label->name }}
                            </span>
                        @endforeach
                        @if($issue->isOpen())
                            <p class="list-group-item-text text-muted issue-meta">
                                #{{ $issue->number}} opened {{ $issue->created_at->diffForHumans()}} by {{ $issue->user()->name }}
                            </p>
                        @else
                            <p class="list-group-item-text text-muted issue-meta">
                                #{{ $issue->number}} opened by {{ $issue->user()->name }}, closed {{ $issue->closed_at->diffForHumans() }}
                            </p>
                        @endif
                    </a>
                @endforeach
            </div>

            </div>

        </div>


        <div class="col-md-3">
            <div class="panel panel-default @if(!$project->isOpen())panel-warning @endif">
                <div class="panel-heading">
                    {{ $project->name }} <!-- <small class="pull-right">{{ $project->slug }}</small> -->
                </div>

                <div class="panel-body">
                    @if (Auth::user()->hasRole('admin'))
                        <a class="pull-right btn btn-default" href="{{ route('project.edit', $project->slug) }}" title="Edit project"><i class="fa fa-pencil"></i></a>
                    @endif
                    <p>@markdown($project->description)</p>
                </div>
                @if($project->isOpen())
                <div class="panel-footer">
                    <a href="{{ route('issue.create', $project->slug ) }}" class="btn btn-primary btn-lg btn-block">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add new issue
                    </a>
                </div>
                @endif
            </div>
        </div>

    </div>
</div>
@endsection
