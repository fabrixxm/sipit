@extends('layouts.nav')

@section('title')
 - {{ $issue->title }} - {{ $project->slug }} / Issue #{{ $issue->number}}
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li><a href="{{ route('project',  $project->slug) }}">{{ $project->slug }}</a></li>
    <li><a href="{{ route('issue', [$project->slug, $issue->number]) }}" class="active">Issue #{{ $issue->number }}</a></li>
@endsection

@section('content')
    <div class="container issue">
        <h1>
            {{ $issue->title }} <span class="text-muted">• #{{ $issue->number }}</span>

            <div class="btn-group pull-right" role="group">
            @if( $issue->canEdit(Auth::user()))
                <a href="{{ route('issue.edit', [$project->slug, $issue->number] )}}" class="btn btn-default">
                    <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                </a>
            @endif
            @if( $issue->isSubscriber(Auth::user()) )
                <a href="{{ route('issue.unwatch', [$project->slug, $issue->number] )}}" class="btn btn-default ">
                    <i class="fa fa-eye-slash" aria-hidden="true"></i> Stop watch
                </a>
            @else
                <a href="{{ route('issue.watch', [$project->slug, $issue->number] )}}" class="btn btn-default ">
                    <i class="fa fa-eye" aria-hidden="true"></i> Watch
                </a>
            @endif
            </div>
        </h1>
        <p>
        @if($issue->isOpen())
            <span class="label label-warning"><i class="fa fa-info-circle" aria-hidden="true"></i> Open</span>
        @else
            <span class="label label-default"><i class="fa fa-check-circle" aria-hidden="true"></i> Closed</span>
        @endif
            <strong>{{ $issue->user()->name }}</strong> created this issue
            <span title="{{ $issue->created_at }}">{{ $issue->created_at->diffForHumans() }}</span>
            &centerdot; comments: {{ $issue->commentsCount() }}
        </p>
        <hr>
        <div class="row">
            <div class="col-md-9 issue-comments">

                @foreach( $comments as $comment)
                    @include('issues.comment.'.$comment->type)
                @endforeach

                <div class="newcomment">
                    @if( $issue->canComment(Auth::user()))
                    <!-- comment element -->
                    <div class="row">
                        <div class="col-sm-1 hidden-xs">
                            <img src="{{ route('avatar', [Auth::user()->id,48]) }}"  class="avatar" width="48" height="48">
                        </div>
                        <div class="col-sm-11">
                            <div class="panel with-nav-tabs panel-default with-avatar editor">
                              <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="editor-btn-write active"><a href="#">Write</a></li>
                                    <li class="editor-btn-preview"><a href="#">Preview</a></li>
                                </ul>
                              </div>
                              {!! BootForm::open(['model'=>null, 'store'=>['comment.add', $project->slug, $issue->id]]) !!}
                                  <div class="panel-body">
                                    <div class="editor-preview"></div>
                                    <textarea rows="6" class="form-control editor-text" id="text" name="text" cols="50"></textarea>
                                  </div>
                                  <div class="panel-footer">
                                      <button type="submit" class="btn btn-primary">Comment
                                          @if(!$issue->isOpen())
                                              and reopen
                                          @endif
                                      </button>
                                      @if($issue->isOpen())
                                          <button type="submit" class="btn btn-success pull-right" name="closing" value="true">Close issue</button>
                                      @endif
                                  </div>
                              {!! BootForm::close() !!}
                            </div>
                        </div>
                    </div>
                    @endif

                </div>


            </div>
            @php($issuelabels = $issue->labels()->get())
            @php($issuelabelsid = $issuelabels->pluck('id')->toArray())
            <div class="col-md-3 issue-sidebar">
                <h5>
                    <div class="dropdown flat">
                        <a class="dropdown-toggle" type="button" id="dropdownMenuLabel"
                         data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Labels <span class="caret"></span>
                        </a>
                        <ul id="popup-label-list" class="dropdown-menu filtrable" aria-labelledby="dropdownMenuLabel">
                            <div class="search-box">
                                <input type="text"  id="popup-label-list-search" placeholder="Label name" class="form-control live-search" />
                            </div>
                            @foreach($project->labels()->get() as $label)
                                <li><a href="#" data-label-id="{{ $label->id}}">
                                    <div class="pull-right label label-default"  style="background-color:#{{ $label->color }};">&nbsp;</div>
                                    {{ $label->name }}
                                    <div class="checkmark @if(!in_array($label->id, $issuelabelsid)) hidden @endif"><i class="fa fa-check" title="label selected"></i></div>
                                </a></li>
                            @endforeach
                            <li class="notfound dropdown-header">
                                new label "<span id='new-label-name'></span>"
                                <input id='new-label-color' type="text" value="" name="label-color" class="pick-a-color">
                                <button id='new-label-create' class="btn btn-default btn-sm">create</button>
                            </li>
                        </ul>
                    </div>
                </h5>
                <div id="label-list">

                    @forelse($issuelabels as $label)
                        <a href="{{ route('project', $project->slug) }}?q={{ Utils::editQuery('',['is'=>'open','label'=>$label->name]) }}"
                            class="label label-default issue-label" style="background-color:#{{ $label->color }}">
                                {{ $label->name }}
                        </a>
                    @empty
                        <div class="text-muted">none yet</div>
                    @endforelse
                    <hr>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts');
    <script>
        (function(){
            /**
             * label popup
             *  - click on a label to add/remove
             *  - search for a new label to create and add it
             */
            var toggleurl = "{{ route('issue.togglelabel', [ $project->slug, $issue->id, '*']) }}";
            var createurl = "{{ route('issue.createlabel', [ $project->slug, $issue->id ]) }}";
            var labellist = $("#popup-label-list");

            labellist.on("click", "a", null,  function(e) {
                e.preventDefault();
                var check = $(this).find(".checkmark");
                $.post(toggleurl.replace("*", $(this).data('label-id')), {}, function(result) {
                    window.location.reload();
                });
            });

            $("#new-label-create").on("click", function() {
                var name = $("#new-label-name").html();
                var color = $("#new-label-color").val();
                $.post(createurl, {'name':name, 'color':color}, function(result) {
                    window.location.reload();
                });
            });


            $("#popup-label-list-search").on('keyup', function() {
                $("#new-label-name").html($(this).val());
            });

            /**
             * Editable comments
             */
            $(".editable").each(function(){
                var $editable = $(this);
                var editableid = this.id;
                var $editor = $("#"+editableid+"-editor").hide();
                $editable.find(".editable-btn-edit").on("click", function(e) {
                    e.preventDefault();
                    $editable.hide();
                    $editor.show();
                });
                $editor.find(".editor-btn-cancel").on("click", function(e) {
                    e.preventDefault();
                    $editable.show();
                    $editor.hide();
                });

            });


        })();
    </script>
@endsection
