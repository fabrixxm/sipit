@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                {!! BootForm::open(['model' => $project, 'store' => 'project.update', 'update' => 'project.update']) !!}
                    <div class="panel-heading">
                        <h3 class="panel-title">@yield('form.title')</h3>
                    </div>
                    <div class="panel-body">
                        {!! BootForm::text('name') !!}
                        <!-- { !! BootForm::textarea('description') !! } -->
                        @include('forms.previewtextarea', ['name'=>'description', 'text'=>(is_null($project) ? "" : $project->description) ])

                        <h4>Users</h4>

                        @php($projectusers = is_null($project) ? [] : $project->users()->pluck('user_id')->toArray())
                        <select class="selectpicker  form-control" multiple
                            data-live-search="true" name="users[]" id="users">
                          @foreach($users as $user)
                          <option
                                value="{{ $user->id }}"
                                {{-- Admin user editing this is always selected and cannot deselect himself --}}
                                @if(in_array($user->id, $projectusers) || $user->id == Auth::user()->id)
                                    selected
                                @endif
                                {{-- @if($user->id == Auth::user()->id) disabled @endif --}}
                                data-content="{{ $user }}"
                            >
                            {{ $user->name }}
                          </option>
                          @endforeach
                        </select>



                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
