@extends('projects.form')

@section('title')
 - new project
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li class="active">new</li>
@endsection

@section('form.title')
    <i class="fa fa-plus-square"></i> New project

@endsection
