@extends('projects.form')

@section('title')
 - {{ $project->slug }} - edit
@endsection

@section('nav')
    <li><a href="{{ url('/') }}">Projects</a></li>
    <li><a href="{{ route('project',  $project->slug) }}">{{ $project->slug }}</a></li>
    <li class="active">edit</li>
@endsection

@section('form.title')
    <i class="fa fa-pencil-square"></i> Edit {{ $project->slug }}
    @if($project->isOpen())
    <button type="submit" class="btn btn-warning pull-right" name="status" value="c">Close</button>
    @else
    <button type="submit" class="btn btn-success pull-right" name="status" value="o">Open</button>
    @endif
@endsection
