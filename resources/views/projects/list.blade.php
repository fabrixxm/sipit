@extends('layouts.app')


@section('nav')
    <li><a href="{{ url('/') }}" class="active">Projects</a></li>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Projects
                @if (Auth::user()->hasRole('admin'))
                    <a class="pull-right btn btn-default btn-xs"
                     href="{{ route('project.create') }}" title="Add new project">
                        <i class="fa fa-plus"></i>
                    </a>
                @endif
                </div>

                <!-- <div class="panel-body">

                </div> -->
                <ul class="list-group">
                @foreach($projects as $project)
                    <a href="{{ route("project", $project->slug) }}" class="list-group-item">
                        <span class="badge">{{ $project->issues()->isOpen()->count() }}</span>
                        <h4 class="list-group-item-heading">{{ $project->name }} <small>{{ $project->slug }}</small></h4>
                        <p class="list-group-item-text">@markdown($project->description)</p>
                    </a>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
