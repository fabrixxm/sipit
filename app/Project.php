<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Cviebrock\EloquentSluggable\Sluggable;

use App\User;

class Project extends Model
{
    use Sluggable;

    /*
     * Project status constants
     */
    const OPEN = 'o';
    const CLOSED = 'c';

    /*
     * Return a query builder with projects filtered
     * by $user.
     *
     * @param App\User $user
     *
     * @return Illuminate\Database\Query\Builder
     */
    static function forUser(User $user) {
        return Project::whereHas('users', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        });
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Scope a query to only include opened projects.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsOpen(Builder $query)
    {
        return $query->where('status', Project::OPEN);
    }


    /**
     * Convenience method to check open status of project
     *
     * @return bool
     */
    public function isOpen()
    {
        return $this->status == self::OPEN;
    }

    /**
     * Get the issues of the project.
     */
    public function issues()
    {
        return $this->hasMany('App\Issue');
    }


    /**
     * Get the labels of this project.
     */
    public function labels()
    {
        return $this->hasMany("App\Label");
    }


    /**
     * The users with permission on this project.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'project_user');
    }

    /**
     * Check if the user can see the project
     *
     * @param App\User $user
     */
    public function canSee(User $user)
    {
        return $this->users()->where('user_id', $user->id)->count() > 0;
    }

}
