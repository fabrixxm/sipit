<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Request;
use App\Issue;

class NewIssue extends Notification
{
    use Queueable;

    private $issue;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($issue)
    {
        $this->issue = $issue;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $issue = $this->issue;
        $project = $issue->project();

        $subject = sprintf("[%s] %s #%s", $project->name, $issue->title, $issue->number);

        $url = route('issue', [$project->slug, $issue->number] );

        $from = sprintf("notifications@%s", explode(":",Request::getHttpHost())[0]);

        $intro = sprintf('%s create new issue #%s "%s":',
                        $issue->user()->name,
                        $issue->number, $issue->title);

        $message = (new MailMessage)
                    ->subject($subject)
                    ->from($from, $issue->user()->username)
                    ->greeting($intro);

        $comment = $issue->getDetailComment();
        if (!is_null($comment)){
            $lines = explode("\n", $comment->text);
            foreach($lines as $line) {
                $message->line($line); // @TODO: markdown?
            }
        }
        $message->action('Go to the issue', $url);
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "from" => $this->issue->user()->toArray(),
            "issue" =>  $this->issue->toArray()
        ];
    }
}
