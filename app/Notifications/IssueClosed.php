<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Request;
use App\Comment;

class IssueClosed extends Notification implements ShouldQueue
{
    use Queueable;

    private $comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $issue = $this->comment->issue();
        $project = $issue->project();

        $subject = sprintf("[%s] %s #%s", $project->name, $issue->title, $issue->number);

        $url = route('issue', [$project->slug, $issue->number] );

        $from = sprintf("notifications@%s", explode(":",Request::getHttpHost())[0]);

        $intro = sprintf('%s closed issue #%s "%s":',
                        $this->comment->user()->name,
                        $issue->number, $issue->title);

        $message = (new MailMessage)
                    ->subject($subject)
                    ->from($from, $this->comment->user()->username)
                    ->greeting($intro);

        $lines = explode("\n", $this->comment->text);
        foreach($lines as $line) {
            $message->line($line); // @TODO: markdown?
        }
        $message->action('Go to the issue', $url);
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "from" => $this->comment->user()->toArray(),
            "issue" =>  $this->comment->issue()->toArray(),
            "comment" => $this->comment->toArray(),
        ];
    }
}
