<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Haleks\Writedown\Parsers\ParsedownParser;
use App\Markdown\CustomMarkdown;


class CustomMarkdownProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app('writedown')->extend('custommarkdown', function($app) {
            return new ParsedownParser(new CustomMarkdown);
        });
    }
}
