<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Debug queries
        // if (App::environment('local') && env('APP_URL') == 'http://localhost') {
        //     Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
        //         Log::debug($query->sql . ' - ' . serialize($query->bindings));
        //         // // filter oauth ones
        //         // if (!str_contains($query->sql, 'oauth')) {
        //         //     Log::debug($query->sql . ' - ' . serialize($query->bindings));
        //         // }
        //     });
        // }
    }
}
