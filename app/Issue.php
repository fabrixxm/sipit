<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Issue extends Model
{
    /*
     * Issue status constants
     */
    const OPEN = 'o';
    const CLOSED = 'c';
    static $STATUSES = ['OPEN', 'CLOSE'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'closed_at'];

    /**
     * Get the user that owns the issue.
     */
    public function user()
    {
        return $this->belongsTo('App\User')->first();
    }

    /**
     * Get the project that owns the issue.
     */
    public function project()
    {
        return $this->belongsTo('App\Project')->first();
    }

    /**
     * Get the comments related to the issue
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the labels of this issue
     */
    public function labels()
    {
        return $this->belongsToMany('App\Label', 'issue_label');
    }

    /**
     * Get the subscribers of this issue
     */
    public function subscribers()
    {
        return $this->belongsToMany('App\User', 'issue_subscribers');
    }

    /**
     * Check if user is subcribed to this issue
     *
     * @param App\User
     * @return bools
     */
    public function isSubscriber(User $user)
    {
        return $this->subscribers()->where('id', $user->id)->count() > 0;
    }

    /**
     * Scope a query to only include opened issues.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsOpen(Builder $query)
    {
        return $query->where('status', Issue::OPEN);
    }

    /**
     * Scope a query to only include closed issues.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsClosed(Builder $query)
    {
        return $query->where('status', Issue::CLOSED);
    }

    /**
     * Convenience method to check open status of issue
     *
     * @return bool
     */
    public function isOpen()
    {
        return $this->status == Issue::OPEN;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->status == Issue::CLOSED;
    }

    /**
     * Check if user can edit the issue
     *
     * @param User $user;
     *
     * @return bool
     */
    public function canEdit(User $user) {
        return $this->project()->isOpen() && ($this->user_id == $user->id || $user->hasRole('admin'));
    }

    /**
     * Check if user can commnet the issue
     *
     * @param User $user;
     *
     * @return bool
     */
    public function canComment(User $user) {
        return $this->project()->isOpen();
    }

    /**
     * Number of text comments of the issue, less the first one
     *
     * @return int
     */
    public function commentsCount() {
        return $this->comments()
                ->where('type', Comment::COMMENT)
                ->where('created_at', ">" , $this->created_at->addSecond())
                ->count();
    }

    /**
     * Get the issue details comment.
     * It's the first comment created in less than a second.
     *
     * @return App\Comment or null
     */
    public function getDetailComment() {
        return $this->comments()
                ->where('type', Comment::COMMENT)
                ->where('created_at', "<=" , $this->created_at->addSecond())
                ->first();
    }

    /**
     * Add user as subscriber
     *
     * @param App\User $user
     */
    public function addSubscriber(User $user) {
        if ($this->isSubscriber($user)) return;
        $this->subscribers()->attach($user->id);
    }

    /**
     * Remove user as subscriber
     *
     * @param App\User $user
     */
    public function delSubscriber(User $user) {
        if (!$this->isSubscriber($user)) return;
        $this->subscribers()->detach($user->id);
    }
}
