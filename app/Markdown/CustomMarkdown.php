<?php
/**
 * # Custom Markdown Parser
 *
 * Extends ParsedownExtra to add features.
 *
 * ## Check list
 *
 * a list with checkboxes, like a todo list.
 *
 *    - [] unchecked
 *    - [ ] another unchecked
 *    - [x] checked
 *    - [X] another cheched
 *
 * ## Link to issues
 *
 * a link to issue with number.
 *
 *    ... #<number>..
 *
 * there must be a space before the #
 * and number must be a valid issue number
 */
namespace App\Markdown;

use ParsedownExtra;

use Illuminate\Support\Facades\Request;
use App\Issue;

class CustomMarkdown extends ParsedownExtra
{

    function __construct()
    {
        $this->InlineTypes['#'] = ['IssueLink'];
        $this->inlineMarkerList .= "#";
        parent::__construct();
    }

    protected function inlineIssueLink($Excerpt)
    {
        //list('text'=>$text, 'context'=>$context) = $Excerpt;
        $text = $Excerpt['text'];
        $context = $Excerpt['context'];

        // there must be a space before
        $pos = strpos($context, $text);
        if($context[$pos-1]!== ' ' && $context[$pos-1]!== '\t'  ) return null;

        if (preg_match("/#(\d+)/", $text, $matches)) {
            $number = $matches[1];

            // qui ho bisogno di sapere in quale ca.. di progetto
            // il testo si trova, perchè le issue sono univoche per
            // (project_id, number).
            // number ce l'ho qui ($number).
            // il progetto arriva da text(), che CustomMarkdown estende per
            // accettare un array con il testo markdown da parsare e
            // l'istanza del progetto in questione. vedi CustomMarkdown::text()

            if (!array_key_exists('project', $this->context)) return null;
            $slug = $this->context['project']->slug;
            $issue = $this->context['project']->issues()->where('number', $number)->first();
            if (is_null($issue)) return null;

            $url = route('issue',['slug'=>$slug, 'number'=>$number]);
            return array(
                'extent' => strlen($matches[0]),
                'element' => array(
                    'name' => 'a',
                    'text' => $matches[0],
                    'attributes' => array(
                        'href' => $url,
                    ),
                ),
            );

        }
    }

    /**
     * List item with checkboxes
     */
    protected function li($lines)
    {
        // do' per scontato che $lines è sempre un array di un elemento.
        $markup = parent::li($lines);
        $checkbox = "";
        $matches = [];
        if (preg_match("/^ *\[( ?|x)\] /i", $markup, $matches)){
            list($match, $status) = $matches;
            $markup = substr($markup, strlen($match));

            $status = ($status == "x") ? "checked" : "";
            $markup = "<input type='checkbox' disabled $status> " . $markup;
        }

        return $markup;
    }

    /**
     * Parse markdown content into HTML.
     *
     * @param (string|array) $content
     *
     * @return string
     *
     * If $content is array, it must be like
     * [
     *      'content' => 'markdown string',
     *      'context' => [
     *          'project' => instance of Project related to text
     *      ]
     * ]
     */
    public function text($content) {
        $context = [];
        if (is_array($content)) {
            //list("content" => $content, "context" => $context) = $content;
            $context = $content['context'];
            $content = $content['content'];
        }
        $this->context = $context;
        return parent::text($content);
    }


}
