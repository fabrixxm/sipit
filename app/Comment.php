<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * An entity in issue thread.
     * Can be:
     * a user COMMENT. `text` is the markdown text
     * a CLOSE action. `text` is not used
     * a REOPENED action. `text` is not used
     * a LABELADDED or LABELDELETED action. `text` is the id of the added label
     * a REFERENCED notice. `text` is the id of referencing comment
     */
    // types of comment
    const COMMENT = "comm";
    const CLOSE = "clos";
    const REOPENED = "reop";
    const REFERENCED = "refd";
    const LABELADDED = "ladd";
    const LABELDELETED = "ldel";

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];


    /**
     * Get the issue that owns the comment.
     */
    public function issue()
    {
        return $this->belongsTo('App\Issue')->first();
    }

    /**
     * Get the user that owns the commnet.
     */
    public function user()
    {
        return $this->belongsTo('App\User')->first();
    }

    /**
     * Check if user can edit the comment
     *
     * @param User $user;
     *
     * @return bool
     */
    public function canEdit(User $user) {
        return $this->issue()->project()->isOpen() && ($this->user_id == $user->id || $user->hasRole('admin'));
    }

    public function isType($type) {
        return $this->type == constant("self::".$type);
    }

    /**
     * Return the label added or deleted
     *
     * @return \App\Label
     */
    public function getLabel() {
        if ($this->type == self::LABELADDED || $this->type == self::LABELDELETED) {
            $labelid = (int) $this->text;
            return \App\Label::find($labelid);
        } else {
            return null;
        }
    }

    /**
     * Return the comment referencing this thread
     *
     * @return \App\Comment
     */
    public function getReferrer() {
        if ($this->type == self::REFERENCED) {
            $commentid = (int) $this->text;
            return Comment::find($commentid);
        }
        return null;
    }

    /**
     * Parse comment markdown to html.
     * Works only if CustomMarkdown is used as parsed, and only
     * with comments of type COMMENT
     *
     * @return string
     */
    public function parsedMarkdown() {
        if ($this->type !== Comment::COMMENT) return "";
        $ctx = [
            'content' => $this->text,
            'context' => [
                'project' => $this->issue()->project()
            ]
        ];
        return app('writedown')->content($ctx)->toHtml();
    }
}
