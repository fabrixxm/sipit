<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Label extends Model
{
    use Sluggable;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the issues tagged with this label
     */
    public function issues()
    {
        return $this->belongsToMany('App\Issue', 'issue_label');
    }

    /**
     * Get the project this label belongs to
     */
    public function project()
    {
        return $this->belongsTo('App\Project')->first();
    }

}
