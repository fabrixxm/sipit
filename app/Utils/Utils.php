<?php
namespace App\Utils;

class Utils {
    public function startsWith($haystack, $needle)
    {
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||
        (substr($haystack, -$length) === $needle);
    }

    /**
     * Tokenize search parameters, including double or single quoted keys.
     *
     * @param string $query
     *
     * @return array
     */
    public function tokenize($query)
    {
        $tokens = [];
        $token = strtok($query,' ');
        while ($token) {
            // find double quoted tokens
            if ($token[0]=='"' || strpos($token,':"')) {
                $token .= ' '.strtok('"').'"';
                #$token = str_replace(':"',':', trim($token,'"'));
            }
            // find single quoted tokens
            if ($token[0]=="'" || strpos($token,":'")) {
                $token .= ' '.strtok("'")."'";
                #$token = str_replace(":'",':', trim($token,"'"));
            }

            $tokens[] = $token;
            $token = strtok(' ');
        }
        return $tokens;
    }

    /**
     * Add or remove items from $query
     *
     * Single query param can be passed as string, eg "order:date"
     * Multiple query params can be passed as array, eg ['order'=>'date', 'is':'closed']
     *
     * @param array|string $value  [key=>value,...] or "key:value" to add, edit or remove from query
     *
     * @return string
     */
    public function editQuery($query, $value) {
        if (is_string($value)) {
            list($k, $v) = explode(":", $value);
            $query = $this->doEditQuery($query, $value, $k, $v);
        }
        if (is_array($value)) {
            foreach ($value as $key => $value) {
                $str = "$key:$value";
                $query = $this->doEditQuery($query, $str, $key, $value);
            }
        }
        return $query;
    }

    private function doEditQuery($query, $value, $k, $v) {
        $k .= ":";
        $keylen = strlen($k);
        $toks = $this->tokenize($query);
        $res = [];
        foreach ($toks as $t) {
            if (substr($t, 0, $keylen) != $k) {
                $res[] = $t;
            }
        }

        if (strpos($v, " ")!==false) {
            $value = $k."%22".$v."%22";
        }

        if ($v!=="") $res[] = $value;
        return implode(" ", $res);
    }
}
