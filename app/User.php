<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Support\Htmlable as HtmlableContract;

use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements HasRoleAndPermissionContract, HtmlableContract
{
    use HasRoleAndPermission;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get the issues created by the user.
     */
    public function issues()
    {
        return $this->hasMany('App\Issue');
    }

    /**
     * Get the issues user is subscribed to.
     */
    public function subscribtions()
    {
        return $this->hasMany('App\Issue', 'issue_subscribers');
    }

    /**
     * Is user subscribed to issue id
     *
     * @param App\Issue $issue
     * @return bool
     */
    public function isSubscribedTo(Issue $issue)
    {
        return $this->subscribtions()->where('id', $issue->id)->count() > 0;
    }


    /**
     * Get the comments created by the user.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the projects this user can access
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project', 'project_user');
    }

    public function toHtml()
    {
        return "@".$this->username;
    }

}
