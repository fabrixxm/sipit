<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserRequest extends AdminRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed',
        ];
    }
}
