<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification; // non dovrebbe essere registrato "global" ?

use App\Http\Requests;
use App\Notifications;
use App\User;
use App\Project;
use App\Issue;
use App\Comment;
use App\Label;
use App\Utils\Utils;


class IssueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->utils = new Utils;
    }


    /**
     * List project's issues
     *
     * @param \Illuminate\Http\Request $request
     * @param string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        $project = $request->user()->projects()->where('slug', $slug)->firstOrFail();

        $query = $request->input("q", "is:open");
        $issues = $project->issues()->withCount(['comments' => function($q){
            $q->where('type', Comment::COMMENT);
        }]);
        $issues = $this->doQuery($issues, $query)->get();

        return view('issues.list', [
            'project' => $project,
            'issues' => $issues,
            'query' => $query,
        ]);
    }


    private function doQuery($objects, &$query)
    {
        $statuses = ['open'=>Issue::OPEN, 'closed'=>Issue::CLOSED];

        $sorted = false;

        $res = [];
        $toks = $this->utils->tokenize($query);

        foreach($toks as $tok) {
            if (strpos($tok, ":")!==false) {
                list($k, $v) = explode(":", $tok, 2);
                $v = trim($v, "\"'");
                switch($k) {
                    case "is":
                        if (array_key_exists($v, $statuses)) {
                            $objects->where('status', $statuses[$v]);
                            $res[] = $tok;
                        }
                        break;

                    case "author":
                        $user = User::where('username', $v)->first();
                        if ($user) {
                            $objects->where('user_id', $user->id);
                            $res[] = $tok;
                        }
                        break;

                    case "label":
                        if ($v === "none") {
                            $objects->has('labels', "=", 0);
                            $res[] = $tok;
                        } else {
                            $label = Label::where('name', $v)->first();
                            if ($label) {
                                $objects->whereHas('labels', function ($query) use ($label) {
                                    $query->where('label_id', $label->id);
                                });
                                $res[] = $tok;
                            }
                        }
                    case "sort":
                        $field = $v;
                        $dir = "asc";

                        if (strpos($v, "-")) {
                            list($field,$dir) = explode("-", $v);
                        }

                        switch($field) {
                            case "created":
                                $field = "created_at";
                                break;
                            case "updated":
                                $field = "updated_at";
                                break;
                            case "comments":
                                $field = "comments_count";
                                break;
                            default:
                                $field = "";
                        }

                        if ($field != "") {
                            $objects->orderBy($field, $dir);
                            $res[] = $tok;
                            $sorted = true;
                        }
                }
            } else {
                #TODO full text seach;
                $res[] = $tok;
            }
        }

        if (!$sorted) $objects->orderBy('created_at', 'asc');

        $query = implode(" ", $res);

        return $objects;
    }


    /**
     * Show issue details
     *
     * @param \Illuminate\Http\Request $request
     * @param string $slug Project slug
     * @param int $number Issue number
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request, $slug, $number) {
        $project = Project::forUser($request->user())->where('slug', $slug)->firstOrFail();
        $issue = $project->issues()->where('number', $number)->firstOrFail();
        $comments = $issue->comments()->orderBy('created_at')->get();

        return view('issues.view', [
            'project' => $project,
            'issue' => $issue,
            'comments' => $comments,
        ]);
    }

     /**
     * Create new issue, only on open projects
     *
     * @param \Illuminate\Http\Request $request
     * @param string $slug Project slug
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $slug) {
        $project = Project::forUser($request->user())->where('slug', $slug)->isOpen()->firstOrFail();

        return view('issues.new', [
             "project"=>$project,
             "issue"=>null
        ]);
    }

     /**
     * Edit issue, only on open projects
     *
     * @param \Illuminate\Http\Request $request
     * @param string $slug Project slug
     * @param int $number Issue number
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $slug, $number) {
        $project = Project::forUser($request->user())->where('slug', $slug)->isOpen()->firstOrFail();
        $issue = $project->issues()->where('number', $number)->firstOrFail();

        if ($issue->user_id != $request->user()->id) {
            abort('403');
        }

        return view('issues.edit', [
            'project' => $project,
            'issue' => $issue,
        ]);
    }


    /**
     * Update or create issue
     *
     * @param \App\Http\Routes\StoreIssueRequest $request
     * @param string $slug Project slug
     * @param optional int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StoreIssueRequest $request, $slug, $id=null)
    {
        $project = Project::forUser($request->user())->where('slug', $slug)->isOpen()->firstOrFail();
        $description = $request->input('description','');
        $notifiable_users = [];

        if (is_null($id)) {
            $issue = new Issue;
            $issue->project_id = $project->id; // mah..
            $last_number = (int) $project->issues()->max('number');
            $issue->number = $last_number + 1;
            $issue->user_id = $request->user()->id; // mah..

            // send notifications
            // only to admins on project
            $notifiable_users = $project->users()->whereHas('roles', function($q) {
                                    $q->where('slug','=','admin');
                                })->where('users.id','!=', $issue->user_id)->get();
       } else {
            $issue = Issue::findOrFail($id);
            if ($issue->user_id != $request->user()->id) {
                abort('403');
            }
            $issue->status = $request->input('status', $issue->status);

        }

        $issue->title = $request->input('title');
        $issue->save();

        if (is_null($id) && $description !== "") {
            Comment::create([
                'type' => Comment::COMMENT,
                'text' => $description,
                'user_id' => $request->user()->id, // mah..
                'issue_id' => $issue->id,
            ]);
        }

        // oh hei, you created this issue, now you're watching it!
        $issue->addSubscriber($request->user());

        // send notification to admins
        if (count($notifiable_users)>0) {
            Notification::send($notifiable_users, new Notifications\NewIssue($issue));
        }

        $request->session()->flash('alert', ['type'=>'success', 'message'=>'Issue saved']);
        return redirect()->route('issue', [ $project->slug, $issue->number ]);
    }

    /**
     * Toggle label on issue
     *
     * @param \Illuminate\Http\Request $request
     * @param string $slug Project slug
     * @param int $issueid Issue id
     * @param int $labelid Label id
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleLabel(Request $request, $slug, $issueid, $labelid)
    {
        $userid = $request->user()->id;
        $project = Project::where('slug', $slug)->isOpen()->firstOrFail();
        $issue = Issue::findOrFail($issueid);
        $label = $project->labels()->findOrFail($labelid);

        if ($issue->labels()->get()->contains($label)) {
            // issue has label, let's remove it
            $issue->labels()->detach($labelid);

            Comment::create([
                'user_id' => $userid,
                'issue_id' => $issueid,
                'text' => $labelid,
                'type' => Comment::LABELDELETED
            ]);
        } else {
            // issue isn't labeled with this label. add it
            $issue->labels()->attach($labelid);

            Comment::create([
                'user_id' => $userid,
                'issue_id' => $issueid,
                'text' => $labelid,
                'type' => Comment::LABELADDED
            ]);
        }

        return "ok"; //view('issues.label.list', ['issuelabels' => $issue->labels()->get()]);
    }

    /**
     * Create a new label and add it to issue
     * POST params: 'name', 'color'
     *
     * @param \App\Http\Requests\StoreLabelRequest $request
     * @param string $slug Project slug
     * @param int $issueid Issue id
     *
     * @return \Illuminate\Http\Response
     */
    public function createLabel(Requests\StoreLabelRequest $request, $slug, $issueid)
    {
        $userid = $request->user()->id;
        $project = $request->user()->projects()->where('slug', $slug)->isOpen()->firstOrFail();
        $issue = Issue::findOrFail($issueid);

        $name = $request->input('name');
        $color = $request->input('color', '777777');

        $label = Label::create([
            'project_id' => $project->id,
            'name' => $name,
            'color' => $color,
        ]);

        $issue->labels()->attach($label->id);

        Comment::create([
            'user_id' => $userid,
            'issue_id' => $issueid,
            'text' => $label->id,
            'type' => Comment::LABELADDED
        ]);

        return "ok";
    }

    public function watch(Request $request, $slug, $number)
    {
        $project = $request->user()->projects()->where('slug', $slug)->isOpen()->firstOrFail();
        $issue = $project->issues()->where('number', $number)->firstOrFail();

        $issue->addSubscriber($request->user());
        $request->session()->flash('alert', ['type'=>'success', 'message'=>'You are now watching this issue']);
        return redirect()->route('issue', [ $project->slug, $issue->number ]);
    }


    public function unwatch(Request $request, $slug, $number)
    {
        $project = $request->user()->projects()->where('slug', $slug)->isOpen()->firstOrFail();
        $issue = $project->issues()->where('number', $number)->firstOrFail();

        $issue->delSubscriber($request->user());
        $request->session()->flash('alert', ['type'=>'success', 'message'=>'You stopped watching this issue']);
        return redirect()->route('issue', [ $project->slug, $issue->number ]);
    }

}
