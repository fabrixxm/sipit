<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

use App\Http\Requests;
use App\Issue;
use App\Comment;
use App\Notifications;

class CommentController extends Controller
{

    /**
     * Add a comment to issue
     *
     * @param \App\Http\Routes\StoreCommentRequest $request
     * @param string $slug Project slug
     * @param int $issueid Issue id
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Requests\StoreCommentRequest $request, $slug, $issueid)
    {

        $userid = $request->user()->id;
        $project = $request->user()->projects()->where('slug', $slug)->isOpen()->firstOrFail();
        $issue = Issue::where('project_id', $project->id)->findOrFail($issueid);

        $notifiable_users = $issue->subscribers->where('id', '!=', $userid);

        $text = $request->input('text');
        $closing = $request->input('closing');

        // close the issue
        if (!is_null($closing)) {
            $issue->status = Issue::CLOSED;
            $issue->closed_at = \Carbon\Carbon::now();
            $issue->closer_id = $request->user()->id;
            $issue->save();

            $comment = Comment::create([
                'user_id' => $userid,
                'issue_id' => $issueid,
                'text' => "",
                'type' => Comment::CLOSE
            ]);

            Notification::send($notifiable_users, new Notifications\IssueClosed($comment));

            if (is_null($text) || $text === "") {
                return redirect()->route('issue', [ $issue->project()->slug, $issue->number ]);
            }
        }

        // reopen the issue
        if (is_null($closing) && $issue->status == Issue::CLOSED) {
            $issue->status = Issue::OPEN;
            $issue->closed_at = null;
            $issue->closer_id = null;
            $issue->save();

            Comment::create([
                'user_id' => $userid,
                'issue_id' => $issueid,
                'text' => "",
                'type' => Comment::REOPENED
            ]);
            
        }

        $comment = Comment::create([
            'user_id' => $userid,
            'issue_id' => $issueid,
            'text' => $text,
            'type' => Comment::COMMENT
        ]);

        $this->addReference($comment);

        Notification::send($notifiable_users, new Notifications\NewComment($comment));

        // oh hei, you commented this issue, now you're watching it!
        $issue->addSubscriber($request->user());

        return redirect()->route('issue', [ $issue->project()->slug, $issue->number ]);
    }

    /**
     * Update a comment
     *
     * @param \App\Http\Routes\StoreCommentRequest $request
     * @param int $id Comment id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StoreCommentRequest $request, $id)
    {
        Log::debug("CommentController::update ", ['id'=>$id]);
        // TODO: should check user permission on issue
        $comment = Comment::findOrFail($id);
        if ( !$comment->canEdit($request->user()) ) {
            return response('Unauthorized', 401);
        }
        $project = $comment->issue()->project();
        if (!$project->canSee($request->user())) {
            return response()->view('errors.404', [], 404);
        }

        $comment->text = $request->input('text');
        $comment->save();

        $this->addReference($comment);

        return redirect()->route('issue', [ $comment->issue()->project()->slug, $comment->issue()->number ]);
    }

    /**
     * Convert input markdown in html. Used in previews
     *
     * POST param "md"
     *
     * @param Illuminate\Http\Request $request
     * @param string $slug Project slug
     *
     * @return html
     */
    public function preview(Request $request, $slug) {
        $project = $request->user()->projects()->where('slug', $slug)->isOpen()->first();
        $md = $request->input('md', '');
        return $this->md2html($md, $project);
    }

    private function md2html($md, $project=null) {
        $ctx = [
            'content' => $md,
            'context' => [
                'project' => $project
            ]
        ];
        $html = app('writedown')->content($ctx)->toHtml();
        return $html;
    }

    private function getLinkedIssues($md, $project) {

        // questo è molto bruttino
        $html = $this->md2html($md, $project);
        $urlpattern = route('issue',['slug'=>$project->slug, 'number'=>"!*!"]);
        $re = '|'.str_replace("!*!", "[0-9]+", $urlpattern).'">#([0-9]+)</a>|';

        preg_match_all($re, $html, $matches, PREG_PATTERN_ORDER);
        $numbers = array_map("intval", $matches[1]);

        return Issue::where('project_id',$project->id)->whereIn('number', $numbers)->get();
    }

    /**
     * add reference comment to referenced issues
     *
     * @param \App\Comment $comment referencer comment
     */
    private function addReference($comment) {
        Log::debug("CommentController::addReference",['comment'=>$comment]);
        $issues = $this->getLinkedIssues($comment->text, $comment->issue()->project());
        Log::debug("CommentController::addReference", ['issues' => $issues]);

        // don't read already existing relations
        foreach($issues as $issue) {
            $exist_reference = $issue->comments()
                ->where("type", Comment::REFERENCED)
                ->where("text", $comment->id)
                ->count() > 0;
            if (!$exist_reference) {
                $comment = Comment::create([
                    'user_id' => $comment->user_id,
                    'issue_id' => $issue->id,
                    'text' => $comment->id,
                    'type' => Comment::REFERENCED
                ]);
            }
        }
    }
}
