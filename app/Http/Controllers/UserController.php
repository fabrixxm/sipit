<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;

use App\Http\Requests\AdminRequest;
use App\Http\Requests\NewUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;

class UserController extends Controller
{
    protected $roles;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $roles =  app(config('roles.models.role'))->all();

        $this->roles = [];
        foreach($roles as $role) $this->roles[$role->id] = $role->name;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdminRequest $request)
    {
        return view('user.index', ['users'=>User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AdminRequest $request)
    {
        return view('user.create',['user'=>null, 'roles'=>$this->roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewUserRequest $request)
    {
        $credentials = [
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];
        $credentials['password'] = Hash::make($credentials['password']);
        try {
            $user = User::create($credentials);
        } catch (Exception $e) {
            $request->session()->flash('alert', ['type'=>'error', 'message'=>'User already exists']);
            return redirect()->route('users.list');
        }

        $request->session()->flash('alert', ['type'=>'success', 'message'=>'User created']);
        return redirect()->route('users.edit', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(AdminRequest $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminRequest $request, $id)
    {
        return view('user.edit',['user'=>User::findOrFail($id), 'roles'=>$this->roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $message = "User saved";
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $password = $request->input('password');
        if ($password !== "") {
            $user->password = Hash::make($password);
            $message .= ", password updated";
        }

        $user->roles()->sync($request->input('roles', []));

        $user->save();

        $request->session()->flash('alert', ['type'=>'success', 'message'=>$message]);
        return redirect()->route('users.edit', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminRequest $request, $id)
    {
        //
    }
}
