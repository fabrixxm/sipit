<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Http\Requests\StoreProjectRequest;
use Illuminate\Http\Request;

use App\Project;
use App\Issue;
use App\User;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the projects list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projects = Project::forUser($request->user())->where('status', Project::OPEN)->get();

        return view('projects.list', ['projects' => $projects]);
    }


    /**
     * Create project
     *
     * @param \App\Http\Requests\AdminRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AdminRequest $request)
    {
        return view('projects.new', ["project"=>null, "users"=>User::all()]);
    }

    /**
     * Edit project
     *
     * @param \App\Http\Requests\AdminRequest $request
     * @param string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminRequest $request, $slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        return view('projects.edit',[
            'project'=>$project,
            'users'=>User::all()
        ]);
    }

    /**
     * Update or create project
     *
     * @param \App\Http\Routes\StoreProjectRequest $request
     * @param optional int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProjectRequest $request, $id=null)
    {
        if (is_null($id)) {
            $project = new Project;
        } else {
            $project = Project::findOrFail($id);
            $project->status = $request->input('status', $project->status);
        }
        $project->name = $request->input('name');
        $project->description = $request->input('description');
        $project->save();

        $project->users()->sync($request->input('users', []));

        $request->session()->flash('alert', ['type'=>'success', 'message'=>'Project saved']);
        return redirect()->route('project.edit', [ $project->slug ]);
    }

}
