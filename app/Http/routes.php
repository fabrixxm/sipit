<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\User;
use Illuminate\Http\Request;


// temp get logout route updating from 5.2 to 5.3
Route::get('/logout', 'Auth\LoginController@logout');
Route::auth();

Route::resource('users', 'UserController');

Route::get('/', 'ProjectsController@index')->name('project.list');
Route::get('/{slug}/edit', 'ProjectsController@edit')->name('project.edit');
Route::get('/project/new', 'ProjectsController@create')->name('project.create');
Route::match(['put', 'post'], '/project/update/{id?}', 'ProjectsController@update')->name('project.update');

Route::get('/{slug}', 'IssueController@index' )->name('project');
Route::get('/{slug}/issue/new', 'IssueController@create' )->name('issue.create');
Route::get('/{slug}/issue/{number}/edit', 'IssueController@edit' )->name('issue.edit');
Route::get('/{slug}/issue/{number}/watch', 'IssueController@watch' )->name('issue.watch');
Route::get('/{slug}/issue/{number}/unwatch', 'IssueController@unwatch' )->name('issue.unwatch');

Route::match(['put', 'post'], '/{slug}/issue/update/{id?}', 'IssueController@update')->name('issue.update'); // 'IssueController@update')->name('issue.update');
Route::get('/{slug}/issue/{number}', 'IssueController@view' )->name('issue');
Route::post('/{slug}/issue/{issueid}/label/create', 'IssueController@createLabel' )->name('issue.createlabel');
Route::post('/{slug}/issue/{issueid}/label/{labelid}', 'IssueController@toggleLabel' )->name('issue.togglelabel');

Route::post('/{slug}/md/preview', 'CommentController@preview' )->name('comment.preview');
Route::post('/{slug}/issue/{issueid}/comment/create', 'CommentController@create')->name('comment.add');
Route::put('/comment/{commentid}', 'CommentController@update')->name('comment.update');



/**
 * TODO: find a proper way to manage avatars
 */


Route::get('/avatar/{userid}-{size}.png', function ($userid, $size) {
    $filesize="-$size";
    $file = public_path()."/avatar/$userid$filesize.png";

    header("Content-type: image/png");
    header("Cache-control: public, max-age=3600");


    if (is_file($file)) {
        echo file_get_contents($file);
    } else {
        $requiredsize=(int)$size;
        $nstripes = 4;


        $user = \App\User::findOrFail($userid);
        //~ $hash = md5(rand());
        $hash = md5($user->email);
        // #30fda0 #9ea8ea #f34345 #3876c7 #34c450 fe
        $colors = substr($hash, 0, $nstripes*6);
        $grad = (hexdec(substr($hash, 30, 2)) / 255) * 360;


        $size = $requiredsize * 1.414213562;
        $step = $size / $nstripes;


        $im = imagecreatetruecolor($size, $size);
        for($stripe=0; $stripe<$nstripes; $stripe++) {
            $color = imagecolorallocate($im,
                hexdec(substr($colors, 6*$stripe, 2)),
                hexdec(substr($colors, 6*$stripe+2,2)),
                hexdec(substr($colors, 6*$stripe+4,2))
            );
            imagefilledrectangle($im,
                0 , $step * $stripe,
                $size, $step * $stripe + $step,
                $color
            );
        }

        $im = imagerotate($im, $grad, $color);

        $center = imagesx($im) / 2;
        $hsize = $requiredsize / 2;
        //~ imagerectangle($im,
            //~ $center-$hsize, $center-$hsize,
            //~ $center+$hsize, $center+$hsize,
            //~ 0
        //~ );
        $im = imagecrop($im, [
            "x" => $center-$hsize,
            "y" => $center-$hsize,
            "width" => $requiredsize,
            "height" => $requiredsize
        ]);

        //~ if (hexdec(substr($colors, 30, 2)) % 2 ) {
            //~ $im = imagerotate($im, 90, $color);
        //~ }

        imagepng($im, $file);
        imagepng($im);
        imagedestroy($im);
    }
    die();
})->name('avatar')->where('userid', '[0-9]+')->where('size', '[0-9]+');


