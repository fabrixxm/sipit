var elixir = require('laravel-elixir');

require("babel-core").transform("code", {
  plugins: ["transform-es2015-modules-umd"]
});


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var bowerDir = './resources/assets/bower/';


elixir(function(mix) {
    // APP
    mix.sass(['app.scss','paneltabs.scss']);
    mix.babel([
        'filtrable-list.js',
        'pick-a-color.js',
        'app.js'
    ], 'public/js/app.js');

    // VENDOR
    mix.styles([
        'font-awesome/css/font-awesome.css',
        'bootstrap/dist/css/bootstrap.css',
        'bootstrap-select/dist/css/bootstrap-select.css',
    ], 'public/css/vendor.css', bowerDir);

    mix.scripts([
            'jquery/dist/jquery.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'bootstrap-select/dist/js/bootstrap-select.min.js'
            ], 'public/js/vendor.js', bowerDir);

    //~ mix.version([
        //~ 'css/app.css', 'js/app.js',
        //~ 'css/vendor.css', 'js/vendor.js'
    //~ ]);

    mix.copy(bowerDir + 'font-awesome/fonts', 'public/fonts');
    mix.copy(bowerDir + 'lato-font/fonts/lato-hairline', 'public/fonts/lato-hairline');
    mix.copy(bowerDir + 'lato-font/fonts/lato-light-italic', 'public/fonts/lato-light-italic');
    mix.copy(bowerDir + 'lato-font/fonts/lato-normal', 'public/fonts/lato-normal');
    mix.copy(bowerDir + 'lato-font/fonts/lato-normal-italic', 'public/fonts/lato-normal-italic');
    mix.copy(bowerDir + 'lato-font/fonts/lato-bold', 'public/fonts/lato-bold');
    mix.copy(bowerDir + 'lato-font/fonts/lato-bold-italic', 'public/fonts/lato-bold-italic');
    mix.copy(bowerDir + 'glyphicons/fonts', 'public/fonts');
});
