# SImple Php Issue Tracker

A simple issue tracker written in PHP with Laravel framework


# Install

```
$ cp .env.example .env
```

Edit `.env` file as required.

```
$ composer install
$ touch database/database.sqlite
$ php artisan migrate
$ php artisan db:seed
$ php artisan key:generate
```

# Dev

```
$ php artisan db:seed --class=DevDataSeed
$ php artisan serve
```

Javascript and css

Install `node`, `gulp` and `bower`

```
$ npm install
$ bower install
$ gulp

or

$ # Run all tasks and minify all CSS and JavaScript...
$ gulp --production

or

$ gulp watch

```

(see https://laravel.com/docs/5.2/elixir)

Create a new table

```
$ php artisan make:model Name --migration
```

Model in `app/Name.php`, class `\App\Name`, migration in `database/migrations/..._create_names_table.php`

Fixtures (or 'seeds')

```
$ php artisan make:seeder NameTableSeeder
```

File in `database/seeds/NameTableSeeder.php`

```
$ php artisan db:seed

or

$ php artisan db:seed --class=NameTableSeeder
```

### notes

- https://github.com/jenssegers/date
- https://silviomoreto.github.io/bootstrap-select
- https://github.com/dwightwatson/bootstrap-form
- https://packagist.org/packages/ultraware/roles
- https://github.com/haleks/writedown/blob/master/docs/how-to-use.md

Icon from https://openclipart.org/detail/277129/coffee-mug-flat

### todo (in more or less important order)

- [x] edit comments
- [x] preview markdown
- [x] `username` field in user table, for filters and mentions
- [ ] per-project labels
- [ ] Full text search
- [x] checkbox lists in markdown
- [ ] links between issues
- [ ] Icon/logo
- [ ] Graphics for no projects, no issues, 404 not found, 403 forbidden
- [ ] https://github.com/vinicius73/Laravel-InstantAvatar
- [ ] https://laravel.com/docs/5.2/routing#route-model-binding
