<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueLabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_label', function (Blueprint $table) {
            $table->increments('id');
            # $table->timestamps();


            $table->integer('issue_id')->unsigned();
            $table->foreign('issue_id')->references('id')->on('issue');

            $table->integer('label_id')->unsigned();
            $table->foreign('label_id')->references('id')->on('label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('issue_label');
    }
}
