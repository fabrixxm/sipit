<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LabelsPerProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('labels', function (Blueprint $table) {
          $table->integer('project_id')->unsigned()->nullable();
        });

        // add exising labels to first project
        DB::Statement('UPDATE labels SET project_id=0');

        Schema::table('labels', function (Blueprint $table) {
          $table->integer('project_id')->unsigned()->nullable(false)->change();
          $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('labels', function (Blueprint $table) {
          $table->dropForeign('projects_project_id_foreign');
          $table->dropColumn("project_id");
        });
    }
}
