<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::create([
            'name' => "Test Project",
            'description' => "A *sample* project",
        ])->users()->sync([1]);
        Project::create([
            'name' => "Another test",
            'description' => "A **closed** project",
            'status' => Project::CLOSED
        ])->users()->sync([1]);
    }
}
