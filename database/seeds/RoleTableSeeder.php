<?php

use Illuminate\Database\Seeder;
use Ultraware\Roles\Models\Role;
use App\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::firstOrCreate([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => '', // optional
            'level' => 1, // optional, set to 1 by default
        ]);

        $user = User::find(1);
        $user->attachRole($adminRole);
        $user->save();

    }
}
