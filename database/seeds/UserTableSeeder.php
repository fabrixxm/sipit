<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'username' => 'admin',
           'name' => "Admin Istrator",
           'email' => "admin@ragno.local",
           'password' => bcrypt('adminadmin'),
       ]);
    }
}
