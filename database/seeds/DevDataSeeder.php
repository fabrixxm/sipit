<?php

use Illuminate\Database\Seeder;

class DevDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProjectsTableSeeder::class);
        $this->call(IssueTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
    }
}
