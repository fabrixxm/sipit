<?php

use Illuminate\Database\Seeder;
use App\Issue;

class IssueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Issue::create([
            'number' => 1,
            'title' => "Modificare come se fosse antani",
            'user_id' => 1,
            'project_id' => 1,
            'status' => Issue::CLOSED,
            'closed_at' => new Date(),
            'closer_id' => 1,
        ]);
        Issue::create([
            'number' => 2,
            'title' => "La supercazzola prematura a sinistra",
            'user_id' => 1,
            'project_id' => 1,
            'status' => Issue::OPEN,
        ]);
    }
}
