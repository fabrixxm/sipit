<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::create([
            'text' => "`antani` è modificabile non come tapioca\n\nVerificare il questore di sopra.",
            'user_id' => 1,
            'issue_id' => 1,
            'type' => Comment::COMMENT
        ]);
        Comment::create([
            'user_id' => 1,
            'issue_id' => 1,
            'type' => Comment::CLOSE
        ]);

        Comment::create([
            'text' => "Dovrebbe prematurare a *destra*",
            'user_id' => 1,
            'issue_id' => 2,
            'type' => Comment::COMMENT
        ]);
    }
}
